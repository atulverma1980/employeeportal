import React from "react";
import http from "../services/httpService";
export default class Employee extends React.Component{
    state={
        data:{},
    };
    async fetchData(){
        let response = await http.get("/empapp/emps");
        let {data}=response;
        this.setState({data:data});
    }
    componentDidMount(){
        this.fetchData();
    }
    showDetail=(id)=>{
        this.props.history.push(`/admin/viewemp/${id}`);
    }
    render(){
        const{data=[],pageInfo={}}=this.state.data;
        const{pageNumber,numOfItems,totalItemCount}=pageInfo;
       
        return <div className="container m-auto">
            <h3>Showing {pageNumber} to {numOfItems} of {totalItemCount}</h3>
            <div className="row bg-info text-center text-dark">
                <div className="col-4"><b>Name</b></div>
                <div className="col-4"><b>Email</b></div>
                <div className="col-4"><b>Detail</b></div>
            </div>
            {data.map((v,index)=><div className="row text-center" key={index}>
            <div className="col-4 border">{v.name}</div>
                <div className="col-4 border">{v.email}</div>
                <div className="col-4 border">
                    {v.role=="ADMIN"?"":
                    <button className="btn btn-secondary btn-sm" onClick={()=>this.showDetail(v.empuserid)}>Detail</button>
                    }
                </div>
            </div>)}
        </div>
    }
}
