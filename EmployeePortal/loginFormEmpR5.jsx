import React from "react";
import auth from "../services/authService";
import http from "../services/httpService";
export default class Login extends React.Component{
    state={
        detail:{email:"",password:""},
    };
    handleChange=(e)=>{
        const{currentTarget:input}=e;
        let s1 = {...this.state};
        s1.detail[input.name]=input.value;
        this.setState(s1);
    }
   async postData(url,obj){
    try{
        let response = await http.post(url,obj);
        let{data}=response;
        auth.login(data);
        if(data.role=="ADMIN"){
            window.location="/admin"
        }else{
            window.location="/emp"
        }
    }catch(ex){
        if(ex.response && ex.response.status===401){
            let errors={};
            errors.email=ex.response.data;
            console.log(errors);
            this.setState({errors:errors})
        }
    }
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        this.postData("/empapp/loginuser",this.state.detail);

    }
    render(){
        const{email,password}=this.state.detail;
        const{errors=null}=this.state;
        return <div className="container text-center bg-light">
            <h2>Welcome to Employee Management Portal!</h2>
            <h5>Login</h5>
            {errors && errors.email && (<span className="text-danger"><b>{errors.email}.Check Username and Password</b></span>)}
            <div className="row my-4">
                <div className="col-4"></div>
                <div className="col-6">
            <div className="form-group row text-center">
                <label className="col-sm-2 col-form-label" >Email iD:-</label>
                <div className="col-sm-6">
                <input type="text" className="form-control" name="email" value={email} 
                onChange={this.handleChange}/>
                </div> 
            </div>
            <div className="form-group row text-center">
                <label className="col-sm-2 col-form-label" >Password:-</label>
                <div className="col-sm-6">
                <input type="text" className="form-control" name="password" value={password} 
                onChange={this.handleChange}/>
                </div> 
            </div>
            </div>
            </div>
                <div className="col-10">
                    <button className="btn btn-primary" onClick={this.handleSubmit}>Submit</button>
                </div>

        </div>
    }
}