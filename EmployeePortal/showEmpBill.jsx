import React from "react";
import http from "../services/httpService";
import {Link} from "react-router-dom";
import authService from "../services/authService";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import BillForm from "./billForm";
export default class ShowBill extends React.Component{
    state={
        bill:[],
        view:-1,
    };
   
    async componentDidMount(){
        let user = authService.getItem();
        try{
        let response = await http.get(`/empapp/empbills/${user.empuserid}`)
        let {data}=response;
        console.log(response);
        this.setState({bill:data,view:-1});
        }catch(ex){
            if(ex.response && ex.response.status===500){
                let errors={}
                errors.message = "No bill Found Please Generate a Bill ";
                this.setState({errors:errors});
            }
        }
    }
    showBill=()=>{
        this.setState({view:1});
    }
    render(){
        const{data=[]}=this.state.bill;
        const{view,errors=null}=this.state;
        return <div className="container">
                <h3 className="text-center">Welcome to the Employee management Portal</h3>
                {errors && <span className="text-danger text-center">{errors.message}</span>}
                <div className="row bg-info text-center text-dark">
                    <div className="col-2 border">ID</div>
                    <div className="col-3 border">Description</div>
                    <div className="col-3 border">Expense Head</div>
                    <div className="col-2 border">Amount</div>
                </div>
                {data.map((v,index)=><div className="row text-center" key={index}>
                <div className="col-2 border">{v.billid}</div>
                    <div className="col-3 border">{v.description}</div>
                    <div className="col-3 border">{v.expensetype}</div>
                    <div className="col-2 border">{v.amount}</div>
                    <div className="col-2 border">
                    {v.expensetype=="Travel" || v.expensetype=="Hotel"?(
                      <Link to={v.expensetype=="Travel"?`/emp/travelbill/${v.billid}`:`/emp/hotelbill/${v.billid}`}>
                          <FontAwesomeIcon icon={faPlus}/></Link>
                    ):""}
                    </div>
                </div>)}
                <Link onClick={()=>this.showBill()}><u>Submit a New Bill</u></Link>
                {view==-1?"": <BillForm/> }
            </div>
    }
}