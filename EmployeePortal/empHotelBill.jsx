import React from "react";
import http from "../services/httpService";
import auth from "../services/authService";
export default class HotelBill extends React.Component{
    state={
        detail:{},
        error:{},
        button:null,
    };
  async componentDidMount(){
      let user = auth.getItem();
      let {id}=this.props.match.params;
        let response = await http.get(`/empapp/hotelbill/${user.empuserid}/${id}`);
        let {data}=response;
        if(data.hotel==undefined){
            let detail1={billid:id,empuserid:data.empuserid,city:"",hotel:"",corpbooking:"",date1:"",date2:"",month1:"",month2:"",year1:"",year2:""}
            this.setState({detail:detail1,message:"No Hotel Stay Detail Found.Please Enter then.",button:false});
        }else{
            let s1 = {...this.state};
 let index1 = data.staystartdate.indexOf('-');
         s1.detail.date1 = data.staystartdate.substring(0,index1);
 let index2 = data.staystartdate.lastIndexOf('-');
         s1.detail.month1 = data.staystartdate.substring(index1+1,index2);
         s1.detail.year1 = data.staystartdate.substring(index2+1);
 let index3 = data.stayenddate.indexOf('-');
         s1.detail.date2 = data.stayenddate.substring(0,index3);
 let index4 = data.stayenddate.lastIndexOf('-');
         s1.detail.month2 = data.stayenddate.substring(index3+1,index4);
         s1.detail.year2 = data.stayenddate.substring(index4+1);
         s1.detail.city=data.city;
         s1.detail.hotel = data.hotel;
         s1.detail.billid = id;
         s1.detail.empuserid = user.empuserid;
         s1.detail.corpbooking = data.corpbooking=="Yes"?true:false;
         s1.button = true;
         s1.message1="Displaying Hotel Bill Details"
        this.setState(s1);
        }
    }
    async postdata(url,obj){
      let response  =await http.post(url,obj);
      this.setState({message2:"Detail have been Successfully added.",message:"",button:true})
    }
    handleChange=(e)=>{
        const{currentTarget:input}=e
        let s1 = {...this.state};
       input.type=="checkbox"?s1.detail[input.name]=input.checked:s1.detail[input.name]=input.value;
        this.handleValidate(e);
        this.setState(s1);
    }
  
    handleSubmit=(e)=>{
        e.preventDefault();
        let s1 = {...this.state};
        let errors=this.validateForm();
        if(this.isValid(errors)){
            let staystartdate=s1.detail.date1+"-"+s1.detail.month1+"-"+s1.detail.year1;
            let stayenddate=s1.detail.date2+"-"+s1.detail.month2+"-"+s1.detail.year2;
       
            let data={};
            data.billid = s1.detail.billid;
            data.empuserid = s1.detail.empuserid;
            data.staystartdate= staystartdate;
            data.stayenddate = stayenddate;
            data.hotel = s1.detail.hotel;
            data.city = s1.detail.city;
            data.corpbooking = s1.detail.corpbooking==true?"Yes":"No";
        this.postdata(`/empapp/hotelbill`,data);
        }else{
            s1.error = errors;
            console.log(s1.detail);
            this.setState(s1);
        }
    }
    isValid=(errors)=>{
        let keys = Object.keys(errors);
        let count = keys.reduce((acc,curr)=>(errors[curr]?acc+1:acc),0);
        return  count==0;
    }
   
    validateForm=()=>{
        const{city,hotel,corpbooking,date1,date2,month1,month2,year1,year2}=this.state.detail;
        let errors={};
        errors.city = this.validateCity(city);
        errors.hotel  =this.validateHotel(hotel);
        errors.date1 = this.validateDate1(date1);
        errors.date2 = this.validateDate2(date2);
        errors.month1 = this.validateM1(month1);
        errors.month2 = this.validateM2(month2);
        errors.year1 = this.validateY1(year1);
        errors.year2 = this.validateY2(year2);
        return errors;

    }
    validateCity=(city)=>
    !city?"Please Enter City":"";
    validateCorp=(corp)=>
    !corp?"Please Choose CorpBooking":"";
    validateDate1=(d1)=>
    !d1?"Please select date":"";
    validateDate2=(d1)=>
    !d1?"Please select date":"";
    validateM1=(m1)=>
    !m1?"Please select Month":"";
    validateM2=(m1)=>
    !m1?"Please select Month":"";
    validateY1=(y1)=>
    !y1?"Please Select Year":"";
    validateY2=(y1)=>
    !y1?"Please Select Year":"";
    validateHotel=(h)=>
    !h?"Please Enter Hotel":"" 
    handleValidate=(e)=>{
        let{currentTarget:input}=e
        let s1 = {...this.state};
        switch (input.name){
            case "city": s1.error.city = this.validateCity(input.value);break;
            case "hotel":s1.error.hotel=this.validateHotel(input.value); break;
            case "date1":s1.error.date1= this.validateDate1(input.value);break;
            case "date2":s1.error.date2=this.validateDate2(input.value);break;
            case "month1":s1.error.month1=this.validateM1(input.value);break;
            case "month2":s1.error.month2=this.validateM2(input.value);break;
            case "year1":s1.error.year1=this.validateY1(input.value);break;
            case "year2":s1.error.year2=this.validateY2(input.value);break;
            default : break;
        }
        this.setState(s1);
    }
    render(){
        const{billid,city,hotel,corpbooking,date1,date2,month1,month2,year1,year2}=this.state.detail;
        const{message=null,message1=null,message2=null,error}=this.state;
        let day=["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20",
                "21","22","23","24","25","26","27","28","29","30"];
        let month=["January","Febuary","March","April","May","June","July","August","September","October","November","December"];
        let year =["2018","2019","2020","2021"];
        return<div className="container text-center bg-light">
        <h2>Welcome to Employee Management Portal!</h2>
        <h5>Hotel Stay Details</h5>
        <h6>Bill Id :- {billid}</h6>
        {message && <span className="text-danger">{message}</span>}
        {message1 && <span className="text-success">{message1}</span>}
        {message2 && <span className="text-success">{message2}</span>}

 <div className="row my-4">
     <div className="col-3"></div>
        <div className="col-6">
           <div className="form-group row text-center">
              <label className="col-sm-4 col-form-label" >Check in Date:-</label>
              <div className="col-sm-6">
                <div className="row">
                  <div className="col-4">
                    <select className="form-control" name="date1" value={date1} onBlur={this.handleValidate}  onChange={this.handleChange}>
                        <option value="">Select Day</option>
                        {day.map(v=><option key={v}>{v}</option>)}
                    </select>
                    {error.date1 ? <span className="text-danger">{error.date1}</span>:""}
                 </div>
                 <div className="col-4">
                    <select className="form-control" name="month1" value={month1} onBlur={this.handleValidate}  onChange={this.handleChange}>
                        <option value="">Select Month</option>
                        {month.map(v=><option key={v}>{v}</option>)}
                    </select>
                    {error.month1 ? <span className="text-danger">{error.month1}</span>:""}
                 </div>
                 <div className="col-4">
                    <select className="form-control" name="year1" value={year1} onBlur={this.handleValidate}  onChange={this.handleChange}>
                        <option value="">Select Year</option>
                        {year.map(v=><option key={v}>{v}</option>)}
                    </select>
                    {error.year1 ? <span className="text-danger">{error.year1}</span>:""}
                 </div>
                </div>
            </div> 
        </div>
        <div className="form-group row text-center">
              <label className="col-sm-4 col-form-label" >Check Out Date:-</label>
              <div className="col-sm-6">
                <div className="row">
                  <div className="col-4">
                    <select className="form-control" name="date2" value={date2} onBlur={this.handleValidate}  onChange={this.handleChange}>
                        <option value="">Select Day</option>
                        {day.map(v=><option key={v}>{v}</option>)}
                    </select>
                    {error.date2 ? <span className="text-danger">{error.date2}</span>:""}
                 </div>
                 <div className="col-4">
                    <select className="form-control" name="month2" value={month2}onBlur={this.handleValidate}  onChange={this.handleChange}>
                        <option value="">Select Month</option>
                        {month.map(v=><option key={v}>{v}</option>)}
                    </select>
                    {error.month1 ? <span className="text-danger">{error.month2}</span>:""}
                 </div>
                 <div className="col-4">
                    <select className="form-control" name="year2" value={year2} onBlur={this.handleValidate}  onChange={this.handleChange}>
                        <option value="">Select Year</option>
                        {year.map(v=><option key={v}>{v}</option>)}
                    </select>
                    {error.year1 ? <span className="text-danger">{error.year2}</span>:""}
                 </div>
                </div>
            </div> 
        </div>
        <div className="form-group row text-center">
        <label className="col-sm-4 col-form-label">Hotel:-</label>
        <div className="col-sm-6">
            <input type="text" name="hotel" value={hotel} onChange={this.handleChange}  className="form-control"/>
        </div>
        {error.hotel ? <span className="text-danger">{error.hotel}</span>:""}
        </div>
        <div className="form-group row text-center">
        <label className="col-sm-4 col-form-label">City:-</label>
        <div className="col-sm-6">
            <input type="text" name="city" value={city} onChange={this.handleChange}  className="form-control"/>
        </div>
        {error.city ? <span className="text-danger">{error.city}</span>:""}
        </div>
        <div className="form-check-inline">
            <input type="checkbox" name="corpbooking" value={corpbooking} checked={corpbooking}  className="form-check-input" onChange={this.handleChange}/>
            <label className="form-check-label">Corpbooking</label><br/>
            {error.corpbooking ? <span className="text-danger">{error.corpbooking}</span>:""}
        </div><br/>
        <button className="btn btn-primary" onClick={this.handleSubmit} disabled={this.state.button}>Submit</button>
    </div>
  </div>
    </div>
    }
}