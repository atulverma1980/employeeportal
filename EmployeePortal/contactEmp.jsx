import React from "react";
import http from "../services/httpService";
import auth from "../services/authService";
export default class Contact extends React.Component{
    state={
        detail:{empuserid:"",mobile:"",address:"",country:"",city:"",pincode:""},
        error:{},
        button:null,
    };
  async componentDidMount(){
      let user = auth.getItem();
        let response = await http.get(`/empapp/empcontact/${user.empuserid}`);
        let {data}=response;
        if(data.mobile==undefined){
            this.setState({detail:data,message:"No Contact Detail Found.Please Enter then.",button:true})
        }else{
        this.setState({detail:data,message1:"Displaying Contact Details",button:false});
        }
    }
    async postdata(url,obj){
      let response  =await http.post(url,obj);
      this.setState({message2:"Detail have been Successfully added.",message:"",button:false})
    }
    handleChange=(e)=>{
        const{currentTarget:input}=e
        let s1 = {...this.state};
        s1.detail[input.name]=input.value;
        this.handleValidate(e);
        this.setState(s1);
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let s1 = {...this.state};
        let errors=this.validateForm();
        if(this.isValid(errors)){
        this.postdata(`/empapp/empcontact/${s1.detail.empuserid}`,s1.detail);
        }else{
            s1.error = errors;
            this.setState(s1);
        }
    }
    isValid=(errors)=>{
        let keys = Object.keys(errors);
        let count = keys.reduce((acc,curr)=>errors[curr]?acc+1:acc,0);
        return  count==0;
    }
    isFormValid=()=>{
        let s1 = {...this.state}
         return s1.button;
    }
    validateForm=()=>{
        const{mobile,address,country,city,pincode}=this.state.detail;
        let errors={};
        errors.mobile = this.validateMobile(mobile);
        errors.address  =this.validateAddress(address);
        errors.country= this.validateCountry(country);
        errors.city = this.validateCity(city);
        errors.pincode = this.validatePincode(pincode)
        return errors;

    }
    validateMobile=(mobile)=>
    !mobile?"Please Enter mobile":mobile.length<11?
    "Mobile no. should be 0-9":isNaN(mobile)?"Mobile no. should be 0-9":"";
    validateAddress=(address)=>
    !address?"Please Enter Address":""
    validateCountry=(country)=>
    !country?"Please Enter Country":"";
    validateCity=(city)=>
    !city?"Please Enter City":"";
    validatePincode=(pincode)=>
    !pincode?"Please Enter Pincode":"";
 
    handleValidate=(e)=>{
        let{currentTarget:input}=e
        let s1 = {...this.state};
        switch (input.name){
            case "mobile": s1.error.mobile = this.validateMobile(input.value);break;
            case "address":s1.error.address=this.validateAddress(input.value); break;
            case "country":s1.error.country = this.validateCountry(input.value);break;
            case "city":s1.error.city= this.validateCity(input.value);break;
            case "pincode":s1.error.pincode=this.validatePincode(input.value);
            default : break;
        }
        this.setState(s1);
    }
    render(){
        const{mobile,address,country,city,pincode}=this.state.detail;
        const{message=null,message1=null,message2=null,error}=this.state;
        return<div className="container text-center bg-light">
        <h2>Welcome to Employee Management Portal!</h2>
        <h5>Your Contact Details</h5>
        {message && <span className="text-danger">{message}</span>}
        {message1 && <span className="text-success">{message1}</span>}
        {message2 && <span className="text-success">{message2}</span>}

        <div className="row my-4">
            <div className="col-4"></div>
            <div className="col-6">
        <div className="form-group row text-center">
            <label className="col-sm-2 col-form-label" >Mobile:-</label>
            <div className="col-sm-6">
            <input type="text" className="form-control" name="mobile" value={mobile} 
            onChange={this.handleChange} onBlur={this.handleValidate}/>
            {error.mobile?<span className="text-danger">{error.mobile}</span>:""}
            </div> 
        </div>
        <div className="form-group row text-center">
            <label className="col-sm-2 col-form-label" >Address:-</label>
            <div className="col-sm-6">
            <input type="text" className="form-control" name="address" value={address} 
            onChange={this.handleChange}onBlur={this.handleValidate}/>
          {error.address?<span className="text-danger">{error.address}</span>:""}
            </div> 
        </div>
        <div className="form-group row text-center">
            <label className="col-sm-2 col-form-label" >City:-</label>
            <div className="col-sm-6">
            <input type="text" className="form-control" name="city" value={city} 
            onChange={this.handleChange}onBlur={this.handleValidate}/>
         {error.city?<span className="text-danger">{error.city}</span>:""}
            </div> 
        </div>
        <div className="form-group row text-center">
            <label className="col-sm-2 col-form-label" >Country:-</label>
            <div className="col-sm-6">
            <input type="text" className="form-control" name="country" value={country} 
            onChange={this.handleChange}onBlur={this.handleValidate}/>
         {error.country?<span className="text-danger">{error.country}</span>:""}
            </div> 
        </div>
        <div className="form-group row text-center">
            <label className="col-sm-2 col-form-label" >Pincode:-</label>
            <div className="col-sm-6">
            <input type="text" className="form-control" name="pincode" value={pincode} 
            onChange={this.handleChange}onBlur={this.handleValidate}/>
         {error.pincode?<span className="text-danger">{error.pincode}</span>:""}
            </div> 
        </div>
        </div>
        </div>
            <div className="col-10">
                <button className="btn btn-primary" onClick={this.handleSubmit} disabled={!this.isFormValid()}>Submit</button>
            </div>

    </div>
    }
}