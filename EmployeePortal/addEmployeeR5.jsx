import React from "react";
import http from "../services/httpService";
export default class AddEmployee extends React.Component{
    state={
        detail:{name:"",email:"",password:"",reEnter:""},
        error:{},
        edit:false,
    }
    async postData(url,obj){
        try{
        let response = await http.post(url,obj);
        this.setState({success:"Employee Added Successfully",edit:true});
        }catch(ex){
            if(ex.response && ex.response.status===400){
                let errors = {}
                errors.name = ex.response.data;
                this.setState({errors:errors,edit:false});
            }
        }
    }
    checkStatus=()=>{
       let s1={...this.state};
       return s1.edit;
    }
    handleChange=(e)=>{
        const{currentTarget:input}=e;
        let s1 = {...this.state};
        s1.detail[input.name]=input.value;
        this.setState(s1);
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let data={};
        let s1 = {...this.state};
        let errors= this.validateForm();
        if(this.isValid(errors)){
            data.name=s1.detail.name;
            data.email = s1.detail.email;
            data.password = s1.detail.password;
            data = {...data,role:"EMPLOYEE"}
            this.postData("/empapp/emps",data);
        }else{
            s1.error=errors;
            this.setState(s1);
        }
    }
    isValid=(errors)=>{
        let keys = Object.keys(errors);
        let count = keys.reduce((acc,curr)=>errors[curr]?acc+1:acc,0);
        return count==0
    }
    validateForm=()=>{
        const{name,email,password,reEnter}=this.state.detail;
        let errors={}
        errors.name=this.validateName(name);
        errors.email=this.validateEmail(email);
        errors.password = this.validatePassword(password);
        errors.reEnter=this.validateRenter(reEnter,password);
        return errors;
    }
    checkPass1=(pass)=>{
        const arr=["A","B","C","D","E","F","G","H","I","J","K","L",
        "M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        for(let i=0;i<26;i++){
           //    console.log(arr.length);
            if(pass.includes(arr[i])){
                return true;
            }
        }
        
    }
    checkPass2=(pass)=>{
        const arr=["A","B","C","D","E","F","G","H","I","J","K","L",
        "M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
        for(let i =0;i<26;i++){
            let ch = arr[i].toLowerCase()
            if(pass.includes(ch)){
                return true;
            }
        }
    }
    checkPass3=(pass)=>{
        for(let i =0;i<=9;i++){
            if(pass.includes(i)){
                return true;
            }
        }
    }
    validateName=(name)=>
    !name?"Please Enter Name Of the Employee.":name.length<8?"Name should have at least 8 character.":""
    validateEmail=(email)=>
    !email?"Please Enter Email of the Employee.":!email.includes('@')?"Please Enter Valid Email Address.":""
    validatePassword=(password)=>
    !password?"Please Enter Password.":password.length<8?"Password should have minimum 8 char.":
    !this.checkPass1(password)?"Password must have a Uppercase Letter":!this.checkPass2(password)?"Password must have a Lowercase letter":
    !this.checkPass3(password)?"Password must have a Digit":"";
    validateRenter=(str1,str2)=>
    !str1?"Please Re-Enter the Password":str1!==str2?"Password do not Match":""

    render(){
        const{name,email,password,reEnter}=this.state.detail;
        const{error,edit}=this.state;
        const{success=null}=this.state
        const{errors=null}=this.state;
        return <div className="container text-center bg-light">
        <h2>Welcome to Employee Management Portal!</h2>
        <h5>Add New Employee</h5>
        {success && <span className="text-success">{success}</span>}
        {errors && <span className="text-success">{errors.name}</span>}
        <div className="row my-4">
            <div className="col-4"></div>
            <div className="col-6">
        <div className="form-group row text-center">
            <label className="col-sm-2 col-form-label" >Name:-</label>
            <div className="col-sm-6">
            <input type="text" className="form-control" name="name" value={name} 
            onChange={this.handleChange} placeholder="Enter the Employee Name"/>
            {error.name?<span className="text-danger">{error.name}</span>:""}
            </div> 
        </div>
        <div className="form-group row text-center">
            <label className="col-sm-2 col-form-label" >Email:-</label>
            <div className="col-sm-6">
            <input type="text" className="form-control" name="email" value={email} 
            onChange={this.handleChange} placeholder="Enter the Employee's email"/>
            {error.email?<span className="text-danger">{error.email}</span>:""}
            </div> 
        </div>
        <div className="form-group row text-center">
            <label className="col-sm-2 col-form-label" >Password:-</label>
            <div className="col-sm-6">
            <input type="text" className="form-control" name="password" value={password} 
            onChange={this.handleChange} placeholder="Enter the password"/>
            {error.password?<span className="text-danger">{error.password}</span>:""}
            </div> 
        </div>
        <div className="form-group row text-center">
            <label className="col-sm-2 col-form-label" ></label>
            <div className="col-sm-6">
            <input type="text" className="form-control" name="reEnter" value={reEnter} 
            onChange={this.handleChange} placeholder="Re-Enter password"/>
            {error.reEnter?<span className="text-danger">{error.reEnter}</span>:""}
            </div> 
        </div>
        </div>
        </div>
            <div className="col-10">
                <button className="btn btn-primary" onClick={this.handleSubmit} disabled={this.checkStatus()}>Add</button>
            </div>

    </div>
    }
}