import React from "react";
import http from "../services/httpService";
export default class ShowDetail extends React.Component{
    state={
        detail:{empuserid:"",department:"",designation:"",manager:""},
        error:{},
        button:null,
    };
  async componentDidMount(){
    let {id} = this.props.match.params;
    if(id){
        let response = await http.get(`/empapp/empdept/${id}`);
        let {data}=response;
        if(data.department==undefined){
            this.setState({detail:data,message:"No Department Detail Found.Please Enter then.",button:true})
        }else{
        this.setState({detail:data,message1:"Displaying Department Details",button:false});
        }
     }else{
         let details={empuserid:"",department:"",designation:"",manager:""}
         this.setState({detail:details,button:true});
     }
    }
    async postdata(url,obj){
      let response  =await http.post(url,obj);
      this.setState({message2:"Detail have been Successfully added.",message:"",button:false})
    }
    handleChange=(e)=>{
        const{currentTarget:input}=e
        let s1 = {...this.state};
        s1.detail[input.name]=input.value;
        this.handleValidate(e);
        this.setState(s1);
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let s1 = {...this.state};
        let errors=this.validateForm();
        if(this.isValid(errors)){
        this.postdata(`/empapp/empdept/${s1.detail.empuserid}`,s1.detail);
        }else{
            s1.error = errors;
            this.setState(s1);
        }
    }
    isValid=(errors)=>{
        let keys = Object.keys(errors);
        let count = keys.reduce((acc,curr)=>errors[curr]?acc+1:acc,0);
        return  count==0;
    }
    isFormValid=()=>{
        let s1 = {...this.state}
        if(s1.button==false){
            return s1.button
        }if(s1.button==true){
            let errors = this.validateForm();
            return this.isValid(errors);
        }
    }
    validateForm=()=>{
        const{department,designation,manager}=this.state.detail;
        let errors={};
        errors.designation = this.validateDesignation(designation);
        errors.department  =this.validateDept(department);
        errors.manager= this.validateManger(manager);
        return errors;

    }
    validateDesignation=(designation)=>
    !designation?"Please Enter Designation":"";
    validateManger=(manager)=>
    !manager?"Please Enter Manager":""
    validateDept=(dept)=>
    !dept?"Please Enter Department":""
 
    handleValidate=(e)=>{
        let{currentTarget:input}=e
        let s1 = {...this.state};
        switch (input.name){
            case "department": s1.error.department = this.validateDept(input.value);break;
            case "designation":s1.error.designation=this.validateDesignation(input.value); break;
            case "manager":s1.error.manager = this.validateManger(input.value);break;
            default : break;
        }
        this.setState(s1);
    }
    render(){
        const{department,designation,manager}=this.state.detail;
        const{message=null,message1=null,message2=null,error}=this.state;
        return<div className="container text-center bg-light">
        <h2>Welcome to Employee Management Portal!</h2>
        <h5>Department Details of New Employee</h5>
        {message && <span className="text-danger">{message}</span>}
        {message1 && <span className="text-success">{message1}</span>}
        {message2 && <span className="text-success">{message2}</span>}

        <div className="row my-4">
            <div className="col-4"></div>
            <div className="col-6">
        <div className="form-group row text-center">
            <label className="col-sm-2 col-form-label" >Department:-</label>
            <div className="col-sm-6">
            <input type="text" className="form-control" name="department" value={department} 
            onChange={this.handleChange} onBlur={this.handleValidate}/>
            {error.department?<span className="text-danger">{error.department}</span>:""}
            </div> 
        </div>
        <div className="form-group row text-center">
            <label className="col-sm-2 col-form-label" >Designation:-</label>
            <div className="col-sm-6">
            <input type="text" className="form-control" name="designation" value={designation} 
            onChange={this.handleChange}onBlur={this.handleValidate}/>
          {error.designation?<span className="text-danger">{error.designation}</span>:""}
            </div> 
        </div>
        <div className="form-group row text-center">
            <label className="col-sm-2 col-form-label" >Manager's Name:-</label>
            <div className="col-sm-6">
            <input type="text" className="form-control" name="manager" value={manager} 
            onChange={this.handleChange}onBlur={this.handleValidate}/>
         {error.manager?<span className="text-danger">{error.manager}</span>:""}
            </div> 
        </div>
        </div>
        </div>
            <div className="col-10">
                <button className="btn btn-primary" onClick={this.handleSubmit} disabled={!this.isFormValid()}>Submit</button>
            </div>

    </div>
    }
}