import React from "react";
import http from "../services/httpService";
import auth from "../services/authService";
export default class BillForm extends React.Component{
    state={
        detail:{description:"",expensetype:"",amount:""},
        expenses:["Travel", "Hotel", "Software", "Communication", "Others"],
        error:{},
        button:false,
    }
    handleChange=(e)=>{
        const{currentTarget:input}=e;
        let s1= {...this.state};
        s1.detail[input.name]=input.value;
        this.setState(s1);
    }
    async postdata(url,obj){
        let response = await http.post(url,obj);
        this.setState({message:"New Bill Successfully Added",button:true});
        window.location="/emp/bills"
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let user = auth.getItem();
        let errors = this.validationForm();
        let s1 = {...this.state};
        if(this.isValid(errors)){
            this.postdata(`/empapp/empbills/${user.empuserid}`,s1.detail)
        }else{
            s1.error = errors;
            this.setState(s1);
        }
    }
    isValid=(errors)=>{
        let keys = Object.keys(errors);
        let count = keys.reduce((acc,curr)=>errors[curr]?acc+1:acc,0);
        return  count==0;
    }
    validationForm=()=>{
        const{description,expensetype,amount}=this.state.detail;
        let errors={};
        errors.description = this.validateDescription(description);
        errors.expensetype = this.validateExpense(expensetype);
        errors.amount = this.validateAmount(amount);
        return  errors;
    }
    validateDescription=(des)=>
    !des?"Please Enter Description":"";
    validateExpense=(expense)=>
    !expense?"Please Select Expense Type":"";
    validateAmount=(amt)=>
        !amt?"Please Enter Amount":isNaN(amt)?"Not a Valid Amount":"";
    

    render(){
        const{description,expensetype,amount}=this.state.detail;
        const{expenses,error,message=null}=this.state;
        return <div className="container bg-light text-center my-4">
         <h3 className="text-center">Enter Detail of the New Bill</h3>
         {message && <span className="text-success">{message}</span>}
              <div className="form-group row text-center">
            <label className="col-sm-4 col-form-label" >Description:-</label>
            <div className="col-sm-4">
            <input type="text" className="form-control" name="description" value={description} 
            onChange={this.handleChange} />
            {error.description?<span className="text-danger">{error.description}</span>:""}
            </div> 
        </div>
        <div className="form-group row text-center">
            <label className="col-sm-4 col-form-label" >Amount:-</label>
            <div className="col-sm-4">
            <select className="form-control" name="expensetype" value={expensetype} onChange={this.handleChange}>
                <option value="">Select Expense type</option>
                {expenses.map(v=><option key={v}>{v}</option>)}
            </select>
            {error.expensetype?<span className="text-danger">{error.expensetype}</span>:""}
            </div> 
        </div>
        <div className="form-group row text-center">
            <label className="col-sm-4 col-form-label" >Amount:-</label>
            <div className="col-sm-4">
            <input type="text" className="form-control" name="amount" value={amount} 
            onChange={this.handleChange} />
            {error.amount?<span className="text-danger">{error.amount}</span>:""}
            </div> 
        </div>
        <button className="btn btn-primary" onClick={this.handleSubmit} disabled={this.state.button}>Add</button>
        </div>
    }
}