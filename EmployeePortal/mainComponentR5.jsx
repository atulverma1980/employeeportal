import React from "react";
import {Route,Redirect,Switch} from "react-router-dom";
import auth from "../services/authService";
import Navbar from "./navbarR5";
import Login from "./loginFormEmpR5";
import Logout from "./logoutR5Emp";
import Employee from "./employeeR5";
import AddEmployee from "./addEmployeeR5";
import ShowDetail from "./showDetailEmp"
import Contact from "./contactEmp";
import ShowBill from "./showEmpBill";
import HotelBill from "./empHotelBill";
import TravelBill from "./empTravelBill";
import NotAllowed from "./notAllowed";
export default class MainComponent extends React.Component{
    render(){
        const user = auth.getItem();
        return <div className="container-fluid">
            <Navbar user={user}/>
            <Switch>
                <Route path="/login"  component={Login}/>
                <Route path="/logout" component={Logout}/>
                <Route path="/admin/viewemp/:id" 
                 render={(props)=>user?user.role=="ADMIN"?<ShowDetail {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
                <Route path="/admin/viewemp" 
                  render={(props)=>user?user.role=="ADMIN"?<Employee {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
                <Route path="/emp/travelbill/:id"
                render={(props)=>user?user.role=="EMPLOYEE"?<TravelBill {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
                <Route path="/emp/hotelbill/:id" 
                  render={(props)=>user?user.role=="EMPLOYEE"?<HotelBill {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
                <Route path="/emp/contact"
                 render={(props)=>user?user.role=="EMPLOYEE"?<Contact {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
                <Route path="/emp/bills"
                render={(props)=>user?user.role=="EMPLOYEE"?<ShowBill {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
                <Route path="/admin/addemp" 
                 render={(props)=>user?user.role=="ADMIN"?<AddEmployee {...props}/>:<Redirect to="/notallowed"/>:<Redirect to="/login"/>}/>
                <Route path="/notallowed" component={NotAllowed}/>
                <Redirect from="/" to="/"/>
            </Switch>
        </div>
    }
}