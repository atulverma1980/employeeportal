import React from "react";
import {Link} from "react-router-dom";
import auth from "../services/authService";
export default class Navbar extends React.Component{

    render(){
        const {user}=this.props;
    return  <nav className="navbar navbar-expand-md navbar-dark bg-dark">
        <Link to="/" className="navbar-brand font-weight-bold">
       Employee Portal
        </Link>
        <div className="">
            <ul className="navbar-nav mr-auto">
             {user && user.role==="ADMIN" &&(
            <li className="nav-item dropdown">
        <Link className="nav-link dropdown-toggle" to="/admin" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true">
          Admin
        </Link>
        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
          <Link className="dropdown-item" to="/admin/addemp">add Employee</Link>
          <Link className="dropdown-item" to="/admin/viewemp">View Employee</Link>
        </div>
      </li>)}
      {user && user.role==="EMPLOYEE" && (
      <li className="nav-item dropdown">
        <Link className="nav-link dropdown-toggle" to="/emp" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          My Portal
        </Link>
        <div className="dropdown-menu" aria-labelledby="navbarDropdown">
          <Link className="dropdown-item" to="/emp/contact">Contact Detail</Link>
          <Link className="dropdown-item" to="/emp/bills">Bills</Link>
        </div>
      </li>)}
            </ul>
        </div>
        <div className="navbar-collapse collapse w-100 order-3 dual-collapse2">
            <ul className="navbar-nav ml-auto">
                {!user && (
                <li className="nav-item">
                    <Link className="nav-link" to="/login">Login</Link>
                </li>)}
                {user && (
                <li className="nav-item">
                    <Link className="nav-link" to="/logout">Logout</Link>
                </li>)}
            </ul>
        </div>
    </nav>
    }
}